# Crearemos un crud para nuestro proyecto "Sistema de emision de certificados"

## Preparando nuestras rutas

### web.php

En el archivo web.php modificar el codigo:

```php
<<?php

//

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', [HomeController::class, 'index'])->name('home');
});

```

### home.blade.php

En el archivo indicado modificar la linea **@extends('layaout.app')** por **@extends('adminlte::page')**, realizar esta operación para todas las vistas.

### crear el archivo admin.php en la ruta .../routes/admin.php

En este archivo agregar las siguientes lineas de codigo:

```php
<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\ProductoController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;

Route::get('', [HomeController::class, 'index']);

Route::group(['middleware' => ['auth', 'admin']], function () {
    Route::get('/admin', [AdminController::class, 'index'])->name('admin.index');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', [HomeController::class, 'index'])->name('home');
});
```

### Crear la carpeta admin dentro del directorio .../resources/views

Detro de esta carpeta crear el archivo index.blade.php, con las siguientes lineas de código:

```php
@extends('adminlte::page')

@section('title', 'Dashboard Administración')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <p>Hola Mundo</p>
@stop

```

### En la carpeta .../config buscar el archivo adminlte.php

Dentro de este archivo, buscar las lineas de código:

```php
'dashboard_url' => '/',
```

Modificar la ruta y debe quedar así:

```php
'dashboard_url' => '/admin',
```

### Modificando la configuracion de la ruta

En el archivo RouteServiceProvider.php, agregar las soguientes lineas de código:

```php
<?php

//

class RouteServiceProvider extends ServiceProvider
{
    //

    public function boot(): void
    {

        //

        $this->routes(function () {

           //
            
            Route::middleware('web','auth')
                ->prefix('admin')
                ->group(base_path('routes/admin.php'));
        });
    }
}
```

### Creando el controlador de Home

Para esto ejecutar el comando:

```sh
php artisan make:controller Admin\HomeController
```

En este archivo escrilir las siguientes lineas de código:

```php
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index(){
        return view('admin.index');
    }
}
```

¡Probar que este funciomando correctamente!

# Crearemos nuestro primer CRUD del proyecto

En este eneplo crearé el CRUD para Certificados

