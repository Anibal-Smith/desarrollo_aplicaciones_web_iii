# AdminLTE en Laravel

[AdminLTE](https://github.com/ColorlibHQ/AdminLTE/releases) es una plantilla de diseño para paneles de administración de sistemas. Es muy popular y se utiliza a menudo con el framework de PHP Laravel para crear paneles de administración para aplicaciones web.

jeroennoten/Laravel-AdminLTE es un paquete de Laravel que facilita la integración de la plantilla de diseño AdminLTE en un proyecto de Laravel. Este paquete proporciona una forma sencilla de añadir la plantilla AdminLTE a tu proyecto de Laravel sin tener que realizar la configuración manualmente.

Para utilizar jeroennoten/Laravel-AdminLTE, primero debes instalar Laravel en tu entorno de desarrollo y luego instalar el paquete a través de Composer. A continuación, puedes utilizar el paquete para integrar fácilmente la plantilla AdminLTE en tus vistas de Laravel.

Este paquete también incluye varias herramientas útiles para personalizar la plantilla AdminLTE y adaptarla a tus necesidades específicas. Por ejemplo, puedes utilizar el paquete para configurar el menú de navegación, cambiar el diseño y la apariencia de la plantilla y agregar widgets y elementos adicionales a tu panel de administración.

## Instalar AdminLTE en Laravel

Para instalar jeroennoten/Laravel-AdminLTE en tu proyecto Laravel debes seguir estos pasos:

* Abre tu terminal y dirígete al directorio raíz de tu proyecto de Laravel.

* Ejecuta el siguiente comando para instalar el paquete a través de Composer:

```sh
composer require jeroennoten/laravel-adminlte
php artisan adminlte:install
```

* Una vez instalado el paquete, debes publicar los archivos de configuración del paquete utilizando el siguiente comando:

```sh
php artisan adminlte:install --only=config
```

## Personalizar las vistas de autenticación en AdminLTE en Laravel

Opcionalmente, y solo para Laravel 7+, el paquete ofrece un conjunto de vistas de autenticación con el look&feel de AdminLTE que se puede utilizar en reemplazo de las proporcionadas por el sistema de autenticación de laravel/ui. Si planeas usar estas vistas deberás ejecutar los siguientes comandos en tu proyecto:

```sh
composer require laravel/ui
php artisan ui bootstrap --auth
php artisan adminlte:install --only=auth_views
npm install && npm run dev
```

## Manejo de roles con Enums en Laravel

Para gestionar los roles de forma sencilla y poder dar acceso sólo a los administradores al panel de AdminLTE, vamos a crear un enum válido para el sistema de migraciones en Laravel.

### UserType.php

```php
<?php

namespace App\Enums;

enum UserType: string
{
    case Admin = 'admin';
    case User = 'user';

    public static function forMigration(): array {
        return collect(self::cases())
            ->map(function ($enum) {
                if (property_exists($enum, "value")) return $enum->value;
                return $enum->name;
            })
            ->values()
            ->toArray();
    }
}
```

Ahora vamos a actualizar la migración de usuarios para utilizar nuestro enum UserType:

### Modificamos el archivo ..._create_users_table.php de la carpeta database/Migrations/...

```php
<?php

use App\Enums\UserType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->enum("user_type", UserType::forMigration())->default(UserType::Admin->value);
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
```

Hagamos también un ajuste en nuestro modelo User para hacer buen uso de nuestra configuración:

### User.php

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'user_type',
        'name',
        'email',
        'password',
    ];


    /// ...


    public function isAdmin(): Attribute
    {
        return new Attribute(
            get: fn () => $this->user_type === \App\Enums\UserType::Admin,
        );
    }
}
```

## Middleware para controlar el acceso de Administradores

Es importante proteger el acceso a la sección de administración de nuestras aplicaciones. En Laravel esta tarea la podemos gestionar fácilmente con Middlewares, creemos uno con el siguiente código:

```sh
php artisan make:middleware IsAdmin
```

### IsAdmin.php

```php
<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class IsAdmin
{
    public function handle(Request $request, Closure $next): Closure|RedirectResponse|Response
    {
        if (! $request->user()->isAdmin()) {
            return redirect()->route('home');
        }
        return $next($request);
    }
}
```

El Middleware IsAdmin lo utilizaremos para proteger las rutas de administración más adelante, pero para ello, necesitamos registrarlo en el Kernel Http de la siguiente forma:

### Kernel.php

```php
protected $routeMiddleware = [
    
    //...
    
    'admin' => \App\Http\Middleware\IsAdmin::class,
];
```

## Crear usuario administrador

Vamos a crear un usuario administrador para gestionar nuestro sistema a través de los seeders de Laravel:

### DataSeeder.php

```php
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        \App\Models\User::factory()->create([
            'user_type' => \App\Enums\UserType::Admin,
            'name' => 'Admin',
            'email' => 'admin@adminlte.com',
        ]);
    }
}
```

Con esto ya lo tenemos todo listo y estamos en condiciones de iniciar controladores y rutas para nuestros administradores.

## Controlador para el panel de AdminLTE

Creemos un controlador AdminController rápidamente, el cual se encargará de mostrar el panel AdminLTE a los administradores del sistema:

```sh
php artisan make:controller AdminController
```

### AdminController.php

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.index');
    }
}
```

## Utilizando el layout de AdminLTE en Laravel

El layout de administración de AdminLTE ya está diseñado, sólo debemos utilizarlo de la siguiente forma, en este caso en nuestro archivo de vista admin/index.blade.php:

### index.blade.php

```php
@extends('adminlte::page')

@section('title', 'Dashboard Administración')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <p>Bienvenido al panel de administración de AdminLTE.</p>
@stop
```

## Definir el sistema de rutas

Nuestro sistema de rutas web será sencillo, pero lo necesario para que tú más adelante puedas extenderlo a tus necesidades:

### web.php

```php
<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth', 'admin']], function () {
    Route::get('/admin', [AdminController::class, 'index'])->name('admin.index');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', [HomeController::class, 'index'])->name('home');
});
```

Como puedes ver, por un lado tenemos la ruta para la sección de administración, la cual está protegida por el middleware admin que hemos desarrollado anteriormente, y por otro lado la ruta home que se supone está pensada para mostrar la interfaz para usuarios identificados que no sean administradores.

## Ajustar el inicio de sesión para los administradores

Si ahora te diriges a la ruta /login y accedes con las credenciales de administrador que hemos creado anteriormente verás que Laravel te deja en la ruta /home, y es normal, necesitamos ajustar algunas cosas para que esto funcione como necesitamos.

Lo primero que necesitamos es actualizar el LoginController con lo siguiente:

### LoginContrller.php

```php
<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{

    // ...

    protected function redirectTo(): string
    {
        if (auth()->user()->isAdmin()) {
            return '/admin';
        }
        return '/home';
    }
}
```

El método redirectTo en combinación con nuestro Accessor isAdmin hacen el trabajo que necesitamos, que es mandar al usuario identificado a la zona que corresponda dependiendo de si es administrador o no.

Lo segundo que necesitamos es ajustar el RouteServiceProvider para añadir la siguiente constante, la cual nos servirá a continuación:

### RouteServiceProvider.php

```php
<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    public const HOME = '/home';

    public const ADMIN = '/admin';
    
    //...
    
}
```

Lo tercero y último que necesitamos es actualizar el middleware RedirectIfAuthenticated de la siguiente forma:

### RedirectIfAuthenticated.php

```php
<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                if (auth()->user()->isAdmin())
                {
                    return redirect(RouteServiceProvider::ADMIN);
                }
                return redirect(RouteServiceProvider::HOME);
            }
        }

        return $next($request);
    }
}
```

A partir de ahora, si vuelves a acceder con el usuario administrador, verás que Laravel te deja en la ruta /admin, desde donde podrás ver el panel de administración de AdminLTE.
